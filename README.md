# L'Entropie du monde

L'entropie du monde est croissante. Celle de mon bureau aussi, mais plus vite.

## Mais keskiya ici

Utilisateur depuis quelques années d'un TBI (Prométéan) puis d'un VPI (Epson) j'ai utilisé successivement Active Inspire (proprio), OpenSankoré (très peu), OpenBoard puis Xournal++
Depuis la rentrée je suis revenu à OpenBoard, les dernières versions ayant apporté une solution pour l'utilisation du bureau étendu avec le stylet interactif. Peut-être que ça marchait déjà il y a deux ou trois ans, mais je n'y étais pas arrivé.

Je mets en vrac ici quelques ressources dont j'ai eu besoin pour mes cours de physique-chimie en collège. C'est forcément le résultat d'une utilisation très personelle d'OpenBoard, pas du tout exhaustif, peut-être buggé mais servez-vous : c'est « open bar »

## Licence

[Licence WTFPL](http://www.wtfpl.net/) par défaut.
