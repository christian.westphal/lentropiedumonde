// Boutons d'affichage (Data, Graph, Split)
document.getElementById('btn-data').addEventListener('click', function() {
    document.getElementById('tableau').style.width = '100%';
    document.getElementById('graphique').style.width = '15px';
    document.getElementById('graphique').classList.add('minimized');
    document.getElementById('tableau').classList.remove('minimized');
});

document.getElementById('btn-graph').addEventListener('click', function() {
    document.getElementById('tableau').style.width = '15px';
    document.getElementById('graphique').style.width = '100%';
    document.getElementById('tableau').classList.add('minimized');
    document.getElementById('graphique').classList.remove('minimized');
});

document.getElementById('btn-split').addEventListener('click', function() {
    document.getElementById('tableau').style.width = '33%';
    document.getElementById('graphique').style.width = '67%';
    document.getElementById('tableau').classList.remove('minimized');
    document.getElementById('graphique').classList.remove('minimized');
});

// Charger un fichier CSV
document.getElementById("btn-open").addEventListener("click", function() {
    document.getElementById("csvFileInput").click();
});

document.getElementById("csvFileInput").addEventListener("change", function(event) {
    const file = event.target.files[0];
    if (!file) return;

    const reader = new FileReader();
    reader.onload = function(e) {
        const text = e.target.result;
        loadCSVData(text);
    };
    reader.readAsText(file);
});

function loadCSVData(csvText) {
    const rows = csvText.split("\n");
    const tbody = document.querySelector("#tableau tbody");
    
    tbody.innerHTML = ""; // Tout vider
    let rowCounter = 1; // Réinitialiser le compteur

    rows.forEach((row) => {
        const columns = row.split(";");
        if (columns.length < 2) return; // Ignorer les lignes incomplètes

        const tr = document.createElement("tr");
        const rowId = `row-${rowCounter}`; // Identifiant unique pour chaque ligne

        // Création des cellules avec des IDs uniques pour chaque cellule
        tr.innerHTML = `
            <td contentEditable="true" id="cell-${rowCounter}-1">${columns[0].trim()}</td>
            <td contentEditable="true" id="cell-${rowCounter}-2">${columns[1].trim()}</td>
            <td id="insrow-${rowCounter}" class="ins-column">
                <img src="ins_row.png" alt="Insérer" class="insert-row" />
            </td>
            <td id="delrow-${rowCounter}" class="del-column">
                <img src="del_row.png" alt="Supprimer" class="delete-row" />
            </td>
        `;

        tbody.appendChild(tr);

        // onclink pour insérer une ligne
        const insertIcon = tr.querySelector('.insert-row');
        insertIcon.addEventListener('click', function() {
            insertNewRow(tr);
        });

        // onclick pour supprimer la ligne
        const deleteIcon = tr.querySelector('.delete-row');
        deleteIcon.addEventListener('click', function() {
            tr.remove();  
            createChart();  
        });
        rowCounter++; 
    });

    addEmptyRow(tbody);
}

let rowCounter = 1; // id de ligne

function addEmptyRow(tbody) {
    const newRow = document.createElement('tr');
    const rowId = `row-${rowCounter}`;

    newRow.innerHTML = `
        <td contentEditable="true" id="cell-${rowCounter}-1"></td>
        <td contentEditable="true" id="cell-${rowCounter}-2"></td>
        <td id="insrow-${rowCounter}" class="ins-column">
            <img src="ins_row.png" alt="Insérer" class="insert-row" />
        </td>
        <td id="delrow-${rowCounter}" class="del-column">
            <img src="del_row.png" alt="Supprimer" class="delete-row" />
        </td>
    `;

    tbody.appendChild(newRow);

    // vclick pour insérer une nouvelle ligne
    const insertIcon = newRow.querySelector('.insert-row');
    insertIcon.addEventListener('click', function() {
        insertNewRow(newRow); 
    });

    // clic pour supprimer la ligne
    const deleteIcon = newRow.querySelector('.delete-row');
    deleteIcon.addEventListener('click', function() {
        if (tbody.children.length > 1) {  // vire pas la dernière ligne couillon !
            newRow.remove();
            createChart(); 
        } else {
            // vidage dernière ligne
            newRow.querySelectorAll('td').forEach(cell => {
                if (cell.contentEditable === "true") cell.textContent = "";
            });
        }
    });
    rowCounter++;
}

// fonction pour insérer une ligne entre deux autres
function insertNewRow(referenceRow) {
    const tbody = referenceRow.parentNode;
    const newRow = document.createElement('tr');
    const rowId = `row-${rowCounter}`;

    newRow.innerHTML = `
        <td contentEditable="true" id="cell-${rowCounter}-1"></td>
        <td contentEditable="true" id="cell-${rowCounter}-2"></td>
        <td id="insrow-${rowCounter}" class="ins-column">
            <img src="ins_row.png" alt="Insérer" class="insert-row" />
        </td>
        <td id="delrow-${rowCounter}" class="del-column">
            <img src="del_row.png" alt="Supprimer" class="delete-row" />
        </td>
    `;

    referenceRow.insertAdjacentElement('afterend', newRow);

    const insertIcon = newRow.querySelector('.insert-row');
    insertIcon.addEventListener('click', function() {
        insertNewRow(newRow); 
    });

    const deleteIcon = newRow.querySelector('.delete-row');
    deleteIcon.addEventListener('click', function() {
        newRow.remove();
        createChart();
    });

    rowCounter++;
}

// Fonction pour vérifier et ajouter une ligne vide si nécessaire
function checkLastRow() {
    const tbody = document.querySelector('#tableau tbody');
    const rows = tbody.querySelectorAll('tr');
    const lastRow = rows[rows.length - 1];
    const cells = lastRow.querySelectorAll('td');

    // vérifie si la dernière ligne contient un truc
    const isLastRowNotEmpty = Array.from(cells)
        .slice(0, 2)
        .some(cell => cell.textContent.trim() !== '');

    if (isLastRowNotEmpty) {
        addEmptyRow(tbody); // si oui ajoute une ligne vide
    }
}

// mise à jour dynamique du tableau
document.querySelector('#tableau').addEventListener('input', checkLastRow);

//  ajoute une ligne vide initiale au démarrage
document.addEventListener('DOMContentLoaded', () => {
    const tbody = document.querySelector('#tableau tbody');
    addEmptyRow(tbody);
});

// Séparateur
let isResizing = false;

document.getElementById('separator').addEventListener('mousedown', function(e) {
    isResizing = true;
    document.body.classList.add('no-transition'); // désactive les transitions CSS pendant le redimensionnement (saccades et mauvais suivi sinon)
    document.addEventListener('mousemove', resize);
    document.addEventListener('mouseup', stopResize);
});

function resize(e) {
    if (isResizing) {
        const mainWidth = document.getElementById('main').offsetWidth;
        const newTableWidth = (e.clientX / mainWidth) * 100;
        const newGraphWidth = 100 - newTableWidth;
        
        document.getElementById('tableau').style.width = `${newTableWidth}%`;
        document.getElementById('graphique').style.width = `${newGraphWidth}%`;
    }
}

function stopResize() {
    isResizing = false;
    document.body.classList.remove('no-transition'); // Réactive les transitions CSS après le redimensionnement
    document.removeEventListener('mousemove', resize);
    document.removeEventListener('mouseup', stopResize);
}

/* ************************************************************************ */

function getTableData() {
    const table = document.querySelector('#tableau table');
    const rows = table.querySelectorAll('tbody tr');
    
    const labels = [];
    const data = [];
    
    rows.forEach(row => {
        const cells = row.querySelectorAll('td');
        // Vérifie si la ligne contient des données
        if (cells[0].textContent.trim() !== '' && cells[1].textContent.trim() !== '') {
            labels.push(cells[0].textContent); // Labels from the first column
            
            // Remplacement de la virgule par un point et conversion en float
            const value = parseFloat(cells[1].textContent.replace(',', '.')) || 0;
            data.push(value); // Data from the second column
        }
    });
    
    return { labels, data };
}

// Crée un graphique en courbe avec les données du tableau
let myChart; // Variable globale pour le graphique

function createChart() {
    const { labels, data } = getTableData(); // Récupère les données du tableau
    const ctx = document.getElementById('myChart').getContext('2d');

    // Vérifie si un graphique existe déjà et le détruit
    if (myChart) {
        myChart.destroy();
    }

    // Détecte le mode sélectionné : points, ligne ou bâton
    const selectedMode = document.querySelector('input[name="toggleDisplay"]:checked').value;

    // Conversion de la virgule en point pour les données en mode points/ligne
    let sortedData = [];
    if (selectedMode === 'points' || selectedMode === 'line') {
        sortedData = data.map((value, index) => {
            const xValue = parseFloat(labels[index].replace(',', '.')); // Conversion des virgules en points
            const yValue = typeof value === 'string' ? parseFloat(value.replace(',', '.')) : value;
            return { x: xValue, y: yValue };
        });

        // Tri des données par valeurs de X pour les modes points et ligne
        sortedData.sort((a, b) => a.x - b.x);
    }

    // Configuration du graphique en fonction du mode
    const config = {
        type: selectedMode === 'bar' ? 'bar' : 'scatter', // Barres pour le mode bâton, points/ligne sinon
        data: {
            labels: selectedMode === 'bar' ? labels : undefined, // Utilise les étiquettes pour bâton
            datasets: [{
                label: 'Données du tableau',
                data: selectedMode === 'bar' ? data : sortedData, // Données triées pour points/ligne, texte pour bâton
                fill: false, // Pas de remplissage sous la courbe
                borderColor: 'rgba(75, 192, 192, 1)',
                tension: 0.1, // Lissage pour les courbes en mode ligne
                showLine: selectedMode === 'line', // Trace une ligne uniquement si "ligne" est sélectionné
                pointStyle: 'cross', // Représente les points par des "+" en mode points/ligne
                radius: 9,
                borderWidth: 3,
                backgroundColor: 'rgba(75, 192, 192, 1)' // Couleur pour les barres en mode bâton
            }]
        },
        options: {
            scales: {
                x: {
                    title: {
                        display: true,
                        text: document.querySelector('th:nth-child(1)').textContent || '(Abscisses)', // Titre abscisses
                    },
                    type: selectedMode === 'bar' ? 'category' : 'linear', // Catégorie pour bâton, linéaire pour les autres
                    beginAtZero: false
                },
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: document.querySelector('th:nth-child(2)').textContent || '(Ordonnées)' // Titre ordonnées
                    }
                }
            },
            animation: {
                duration: 300,
                easing: 'linear'
            },
            plugins: {
                legend: {
                    display: false // pas de légende
                }
            }
        }
    };

    // Crée le nouveau graphique
    myChart = new Chart(ctx, config);
}

// clic sur le type dde graphique
document.querySelectorAll('input[name="toggleDisplay"]').forEach((radio) => {
    radio.addEventListener('change', function() {
        createChart();
    });
});

createChart();

// changement dans le tableau  --> MàJ graphique
document.querySelector('#tableau').addEventListener('input', () => {
    createChart(); // Met à jour le graphique chaque fois que le tableau change
});


